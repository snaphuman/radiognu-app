# Aplicación para Firefox OS de RadioÑú #

Este es el repositorio de la aplicación de RadioÑú diseñada para funcionar
nativamente bajo el sistema operativo Firefox OS

## Construyendo desde la fuente ##

Si deseas ayudar con código, revisa el archivo `CONTRIBUTING.md`

## Preguntas Frecuentes ##

### No se guarda la configuración ###

Es probable que el navegador que uses sea un poco antiguo o no estándar y no
soporte [LocalStorage](http://caniuse.com/#feat=namevalue-storage).

Hay reportes de que eso sucede también con Iceweasel, para ese caso, es
necesario ir a la url `about:config` que abre la configuracion avanzada del
navegador y buscar por la variable `dom.storage.enabled`; es necesario que tenga
el valor `true` para que LocalStorage pueda funcionar.

### No encuentro la opción para el efecto de desenfoque del arte de portada ###

Eso es porque tu versión de Firefox no soporta la regla css `filter:blur()`,
introducida recientemente en la v35. La única solución conocida sería actualizar
el sistema operativo completo.
