/*
 * main.js -- Lógica principal de la aplicación
 *
 * Copyright 2013 Felipe Peñailillo <breadmaker@radiognu.org>
 *                William Cabrera   <cabrerawilliam@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

var timer = new Date();
var firstTime = true;
var size = [];
var socket;

function initSocket() {
    try {
        socket = io.connect("http://flows.liquidsoap.fm", {
            transports: ["websocket", "htmlfile", "xhr-polling",
                "jsonp-polling"
            ]
        });
        socket.emit('join', "e710da7b9e83debd5dcb4a5455e9998caba8fca7");
        socket.on('joined', function (data) {
            var tmp = new Date() - timer;
            if (!firstTime) {
                consoleLog("Se conectó al socket a los " + tmp / 1000 +
                    " segundos desde iniciado el script.");
                getInfo({
                    artist: data.artist,
                    title: data.title
                });
            } else {
                $("#init-loading").html($("<i/>").addClass(
                    "icon-spin icon-receiving").css("line-height",
                    "1.6em"));
                consoleLog("Se conectó al socket a los " + tmp / 1000 +
                    " segundos");
            }
        });
        socket.on('error', function (data) {
            consoleLog("Ha ocurrido un error con la conexión: " + data + ".");
            statusMsg({
                message: "Error de socket",
                icon: "icon-error",
                hide: false
            });
        });
        socket.on('disconnect', function () {
            consoleLog("Se ha perdido la conexión con el socket.");
            statusMsg({
                message: "Desconectado",
                icon: "icon-frown",
                hide: false
            });
        });
        socket.on('reconnecting', function () {
            consoleLog("Se ha iniciado la reconexión con el socket.");
            statusMsg({
                message: "Conectando",
                icon: "icon-connecting icon-spin",
                hide: false
            });
        });
        socket.on('reconnect', function () {
            consoleLog("La reconexión con el socket ha sido exitosa.");
            socket.emit('join', "e710da7b9e83debd5dcb4a5455e9998caba8fca7");
            statusMsg({
                message: "Conectado",
                icon: "icon-ok",
                hide: true
            });
        });
        socket.on('reconnect_failed', function () {
            consoleLog("La reconexión con el socket ha fallado.");
            statusMsg({
                message: "Desconectado",
                icon: "icon-frown",
                hide: false
            });
        });
        socket.on("e710da7b9e83debd5dcb4a5455e9998caba8fca7", function (data) {
            if (data.cmd === "metadata") {
                getInfo({
                    "artist": data.radio.artist,
                    "title": data.radio.title
                });
                consoleLog("Recibidos nuevos metadatos: " + JSON.stringify(
                    data));
            } else if (data.cmd === "ping radio") {
                consoleLog("Recibida solicitud de ping desde servidor: " +
                    JSON.stringify(data));
            }
        });
    } catch (e) {
        $("#init").addClass("error");
        $("#init-loading").html($("<i/>").addClass("icon-error text-error")).append(
            " Falló la conexión con el socket").css("font-size", "19px");
    }
}

function initCatalog() {
    $.getJSON("http://radiognu.org/api/?catalog").done(function (data) {
        $("#catalog-wrapper").html($("<div id='catalog-main'/>"));
        /* Usando clumpy para recorrer de forma asincrona el JSON */
        var catalog_clumpy = new Clumpy();
        var catalog_length = data.length;
        var i = 0;
        catalog_clumpy.while_loop(function () {
            return i < catalog_length;
        }, function () {
            if (data[i].artists.length === 1) {
                data[i].artist = data[i].artists;
            } else {
                data[i].artist = data[i].artists.join(", ");
            }
            $("#catalog-main").append(ich.album(data[i]));
            i += 1;
        });
        /* Agregando evento a cada album para cargar sus canciones */
        $("#catalog-main").on("show.bs.collapse", ".album-songs", function () {
            var that = $(this);
            if (!that.data("loaded")) {
                that.html($("<div class='text-center'/>").html($(
                    "<i class='icon-loading icon-spin'/>")).append(
                    " Obteniendo..."));
                $.getJSON("http://radiognu.org/api/", {
                    album: that.data("id")
                }, function (data) {
                    that.empty();
                    $.each(data, function (i, song) {
                        that.append(ich.song(song));
                    });
                    that.data({
                        loaded: true
                    });
                });
            }
        });
        var previewPlaybackInterval;
        /* Agregando evento para cargar el preview de una canción */
        $("#catalog-main").on("click", ".preview-song", function () {
            var that = $(this);
            if (!that.hasClass("hasPreview")) {
                that.button("loading");
                that.addClass("hasPreview");
                $.get("http://radiognu.org/api/", {
                    preview: that.parents(".song").data("id")
                }).done(function (data) {
                    var preview_audio = $("<audio/>").attr({
                        class: "hide",
                        controls: "controls",
                        autoplay: "autoplay",
                        mozaudiochannel: "content",
                        src: data.preview
                    }).on("play", function () {
                        var audioElement = this;
                        $(audioElement).parents(
                            ".album-songs").find(
                            ".catalog-preview-progress"
                        ).css("width", 0);
                        clearInterval(
                            previewPlaybackInterval);
                        previewPlaybackInterval =
                            setInterval(function () {
                                $(audioElement).parents(
                                    ".song").find(
                                    ".catalog-preview-progress"
                                ).css("width",
                                    audioElement
                                    .currentTime /
                                    audioElement
                                    .duration *
                                    100 + "%");
                            }, 100);
                        $("i.icon-preview-stop").parent()
                            .button("reset");
                        that.button("playing");
                        $("#u5p-html5-player audio").finish()
                            .animate({
                                volume: 0
                            }, {
                                queue: false,
                                duration: 500
                            });
                        $(".song-preview audio").not(
                            this).each(function () {
                            var other_preview =
                                $(this)[0];
                            $(other_preview).animate({
                                    volume: 0
                                }, 250,
                                function () {
                                    other_preview
                                        .pause();
                                    other_preview
                                        .currentTime =
                                        0;
                                    other_preview
                                        .volume =
                                        1;
                                });
                        });
                    }).on("ended", function () {
                        clearInterval(
                            previewPlaybackInterval);
                        $(this).parents(".song").find(
                            ".catalog-preview-progress"
                        ).width(0);
                        that.button("reset");
                        $("#u5p-html5-player audio").animate({
                            volume: 1
                        }, {
                            queue: false,
                            duration: 500
                        });
                    });
                    that.after($("<span class='song-preview'/>")
                        .html(preview_audio));
                }).fail(function () {}).always(function () {});
            } else {
                $("i.icon-preview-stop").parent().button("reset");
                var preview_audio = that.next(".song-preview").find(
                    "audio")[0];
                if (preview_audio.paused) {
                    preview_audio.currentTime = 0;
                    preview_audio.volume = 1;
                    preview_audio.play();
                } else {
                    $(preview_audio).parents(".song").find(
                        ".catalog-preview-progress").css("width", 0);
                    clearInterval(previewPlaybackInterval);
                    that.button("reset");
                    $(preview_audio).animate({
                        volume: 0
                    }, 250, function () {
                        preview_audio.pause();
                        preview_audio.currentTime = 0;
                        preview_audio.volume = 1;
                    });
                    $("#u5p-html5-player audio").animate({
                        volume: 1
                    }, {
                        queue: false,
                        duration: 500
                    });
                }
            }
        });
        initOmnisearch(data);
    }).fail(function () {
        setTimeout(function () {
            initCatalog();
        }, 1000);
    });
}

function initOmnisearch(data) {
    var albums = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.name);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: data
    });
    albums.initialize();
    $("#omnisearch-term").typeahead({
        highlight: true
    }, {
        name: "omnisearch",
        displayKey: "name",
        source: albums.ttAdapter(),
        templates: {
            header: "<h5>Álbumes</h5><hr/>",
            empty: ['<div class="empty-message text-center">',
                '<i class="icon-frown"></i> No hay resultados', '</div>'
            ].join('\n'),
            suggestion: Handlebars.compile('<div>{{name}} – {{year}}</div>')
        }
    }).on("typeahead:selected", function (ev, data) {
        var element = $(".album-info[data-target='#album-" + data.id + "']");
        $('#catalog-wrapper').animate({
            scrollTop: element.offset().top - $('#catalog-wrapper').offset()
                .top + $('#catalog-wrapper').scrollTop()
        }, 500);
        $(this).blur();
        if (element.hasClass("collapsed")) element.click();
    }).on("input", function () {
        if ($(this).val() !== "" && $("#omnisearch .icon-search").hasClass("in")) {
            $("#omnisearch .icon-backspace").addClass("in");
            $("#omnisearch .icon-search").removeClass("in");
            $("#omnisearch-backspace-click-space").removeClass("hide");
        } else if ($(this).val() === "" && $("#omnisearch .icon-backspace").hasClass(
                "in")) {
            $("#omnisearch .icon-backspace").removeClass("in");
            $("#omnisearch .icon-search").addClass("in");
            $("#omnisearch-backspace-click-space").addClass("hide");
        }
    });
    $("#omnisearch-backspace-click-space").click(function () {
        if ($("#omnisearch-term").val() !== "") {
            $("#omnisearch")[0].reset();
            $("#omnisearch-term").trigger("input");
        }
    });
}

var sources = ["http://audio.radiognu.org/radiognuam.ogg",
    "http://audio.radiognu.org/radiognu2.ogg",
    "http://audio.radiognu.org/radiognu.ogg"
];

function initOptions() {
    if (!Modernizr.localstorage) {
        $('#config-error').removeClass('hide');
    }
    $("#cover-quality-option").change(function () {
        var value = $(this).val();
        if (Modernizr.cssfilters) {
            if (value === "1" || value === "0") {
                $('#config-cover-blur').removeClass('hide');
            } else {
                $('#config-cover-blur').addClass('hide');
            }
        }
        localStorage.setItem("cover_quality", value);
    });
    if (localStorage.getItem("cover_quality") !== null) {
        $("#cover-quality-option").val(localStorage.getItem("cover_quality")).change();
    } else {
        localStorage.setItem("cover_quality", $("#cover-quality-option").val());
    }
    if (Modernizr.cssfilters) {
        $("#cover-blur-yes").click(function () {
            if ($(this).hasClass("btn-default")) {
                $("#cover-blur-yes").removeClass("btn-default").addClass(
                    "btn-success");
                $("#cover-blur-no").removeClass("btn-danger").addClass(
                    "btn-default");
                localStorage.setItem("cover_blur", "yes");
            }
        });
        $("#cover-blur-no").click(function () {
            if ($(this).hasClass("btn-default")) {
                $("#cover-blur-yes").removeClass("btn-success").addClass(
                    "btn-default");
                $("#cover-blur-no").removeClass("btn-default").addClass(
                    "btn-danger");
                localStorage.setItem("cover_blur", "no");
            }
        });
        if (localStorage.getItem("cover_blur") !== null) {
            $("#cover-blur-" + localStorage.getItem("cover_blur")).click();
        } else {
            localStorage.setItem("cover_blur", "no");
        }
    }
    if (localStorage.getItem("player_quality") !== null) {
        $("#player-quality-option").val(localStorage.getItem("player_quality"));
        source = sources[localStorage.getItem("player_quality")];
        $("#u5p-html5-player audio").attr("src", sources[localStorage.getItem(
            "player_quality")]);
    }
    $("#player-quality-option").change(function () {
        localStorage.setItem("player_quality", $(this).val());
        resetPlayer(sources[$(this).val()]);
    });
    // Verifica por soporte de notificaciones (y soporta la API antigua de
    // Gecko, mozNotification)
    if (!!window.Notification || !!navigator.mozNotification) {
        // Error conocido: Al activamente denegar la autorización para permitir
        // notificaciones, no hay forma (conocida) de volver a preguntar por
        // autorización, a menos que el usuario cambie la configuración de
        // permisos en su navegador.
        $("#notification-option-yes").click(function () {
            if ($(this).hasClass("btn-default")) {
                if ("mozNotification" in navigator) {
                    localStorage.setItem("notifications", "yes");
                    navigator.mozNotification.createNotification("RadioÑú",
                        "Notificaciones activadas").show();
                } else if ("Notification" in window) {
                    if (Notification.permission !== 'granted') {
                        Notification.requestPermission(function (result) {
                            if (result === 'denied' || result ===
                                'default') {
                                $("#notification-option-no").click();
                                localStorage.setItem("notifications",
                                    "no");
                            } else {
                                localStorage.setItem("notifications",
                                    "yes");
                            }
                        });
                    } else {
                        localStorage.setItem("notifications", "yes");
                    }
                }
            }
            $("#notification-option-yes").removeClass("btn-default").addClass(
                "btn-success");
            $("#notification-option-no").removeClass("btn-danger").addClass(
                "btn-default");
        });
        $("#notification-option-no").click(function () {
            if ($(this).hasClass("btn-default")) {
                $("#notification-option-yes").removeClass("btn-success").addClass(
                    "btn-default");
                $("#notification-option-no").removeClass("btn-default").addClass(
                    "btn-danger");
                localStorage.setItem("notifications", "no");
            }
        });
        $("#config-notification").removeClass("hide");
        if (localStorage.getItem("notifications") !== null) {
            $("#notification-option-" + localStorage.getItem("notifications")).click();
        }
    }
}

var statusHideTimeout;

function statusMsg(config) {
    clearTimeout(statusHideTimeout);
    if ($("#status i").hasClass(config.icon)) {
        $("#status").addClass("in").find("#status-message").html(" " + config.message);
    } else {
        $("#status").addClass("in").html($("<i/>").addClass(config.icon)).append($(
            "<span id='status-message'/>").html(" " + config.message));
    }
    if (config.hide) {
        statusHideTimeout = setTimeout(function () {
            $("#status").removeClass("in");
        }, 5000);
    }
}

//~ function reconnectSocket(){
//~ if(socket!=undefined){
//~ }
//~ }

function fetchInfo(metadata) {
    if (!firstTime) $(".progress").addClass("progress-striped active");
    $.ajax({
        dataType: "json",
        url: "http://radiognu.org/api/?img_size=" + ((localStorage.getItem(
            "cover_quality") !== null) ? size[localStorage.getItem(
            "cover_quality")] : size[3]),
        data: metadata
    }).done(function (data) {
        if ($("#init").length == 1) $("#init").removeClass("in").on(
            "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
            function () {
                $(this).remove();
            });
        if (firstTime) {
            $("#cover").css('background-image', 'url(' + data.cover + ')').removeClass(
                function (index, css) {
                    return (css.match(/\bquality-\S+/g) || []).join(' ');
                }).toggleClass('quality-' + localStorage.getItem(
                    'cover_quality'), localStorage.getItem("cover_blur") ===
                "yes");
            $("#title").html(data.title);
            $("#artist").html(data.artist + ' (' + data.country + ')');
            $("#album").text(data.album + ' (' + data.year + ')');
            $("#license").text(data.license.shortname);
            $("#listeners-quantity").text(data.listeners);
            if (data.isLive) {
                $("#live").removeClass("hidden").addClass("in");
            }
            document.title = [data.artist, data.title].join(" - ");
            initSocket();
            firstTime = false;
        } else {
            if (localStorage.getItem("notifications") === "yes") {
                if ("mozNotification" in navigator) {
                    // Error conocido: La API antigua no tiene ni opción tag,
                    // ni método close(), lo que hace que la lista de
                    // notificaciones se llene rápidamente
                    navigator.mozNotification.createNotification(data.title,
                            data.artist + " (" + data.country + ")\n" + data.album +
                            " (" + data.year + ")" + (data.license.shortname !==
                                undefined ? "\n" + data.license.shortname : ""))
                        .show();
                } else if (Notification.permission === 'granted') {
                    var n = new Notification(data.title, {
                        body: data.artist + " (" + data.country + ")\n" +
                            data.album + " (" + data.year + ")" + (data.license
                                .shortname !== undefined ? "\n" + data.license
                                .shortname : ""),
                        icon: "noicon",
                        tag: "radiognu"
                    });
                    setTimeout(function () {
                        n.close();
                    }, 5000);
                }
            }
            $('#player-info').collapse("hide").one("hidden.bs.collapse",
                function () {
                    $("#title").html(data.title);
                    $("#artist").html(data.artist + ' (' + data.country +
                        ')');
                    $("#album").text(data.album + ' (' + data.year + ')');
                    $("#license").text((data.license.shortname !== undefined) ?
                        data.license.shortname : "");
                    $("#listeners-quantity").text(data.listeners);
                    if (data.isLive) {
                        $("#live").removeClass("hidden");
                        setTimeout(function () {
                            $("#live").addClass("in");
                        }, 10);
                    } else {
                        $("#live").one("bsTransitionEnd", function () {
                            $("#live").addClass("hidden");
                        }).removeClass("in");
                    }
                    document.title = [data.artist, data.title].join(" - ");
                    $(this).collapse("show");
                });
            $('#cover').removeClass("in").on(
                "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
                function () {
                    $(this).css('background-image', 'url(' + data.cover +
                        ')').removeClass(function (index, css) {
                        return (css.match(/\bquality-\S+/g) || []).join(
                            ' ');
                    }).toggleClass('quality-' + localStorage.getItem(
                        'cover_quality'), localStorage.getItem(
                        "cover_blur") === "yes").addClass('in');
                    // Corrige un error de visualizacion cuando se actualiza la
                    // informacion de la seccion #player-page y esta no se
                    // encuentra visible.
                }).filter(":not(:visible)").css("background-image", "url(" +
                data.cover + ")").removeClass(function (index, css) {
                return (css.match(/\bquality-\S+/g) || []).join(' ');
            }).addClass('quality-' + localStorage.getItem('cover_quality'),
                localStorage.getItem("cover_blur") === "yes").addClass('in');
            $(".progress").removeClass("progress-striped active");
        }
        if (metadata === undefined) {
            var tmp = new Date() - timer;
            consoleLog("Se recibieron datos desde la API a los " + tmp / 1000 +
                " segundos");
        }
    }).fail(function () {
        if (firstTime) {
            $("#init").addClass("error");
            $("#init-loading").html($("<i/>").addClass("icon-error text-error"))
                .append(" Falló la conexión con la API").css("font-size", "20px");
            var tmp = new Date() - timer;
            consoleLog("Falló la conexión con la API a los " + tmp / 1000 +
                " segundos desde iniciado el script");
        } else {
            setTimeout(function () {
                consoleLog(
                    "Falló la conexión con la API. Reintentando...");
                fetchInfo(metadata);
            }, 1000);
        }
    });
}

function getInfo(metadata) {
    if (metadata === undefined) {
        fetchInfo();
    } else if ($("#title").text() != metadata.title && $("#artist").text() !=
        metadata.artist) {
        fetchInfo(metadata);
    }
}

function getWindowMinSize() {
    size[3] = ($(window).outerWidth(true) < $(window).outerHeight(true)) ?
        $(window).outerWidth(true) : $(window).outerHeight(true);
    size[2] = Math.round(size[3] / 2);
    size[1] = Math.round(size[3] / 4);
    size[0] = Math.round(size[3] / 8);
}

$(document).ready(function () {
    /* Cargando en CSS de forma asincrona */
    $("head").append($("<link/>").attr({
        rel: "stylesheet",
        href: "css/main.css"
    }));
    consoleLog("El documento disparó el evento ready a los " + (new Date() -
        timer) + " milisengundos.");
    /* Inicializando noUISlider */
    $("#u5p-volume-slider").noUiSlider({
        range: {
            'min': 0,
            'max': 100
        },
        start: 100,
        handles: 1
    }).on("slide", function () {
        audio.volume = $(this).val() / 100;
    });
    /* Inicializando plantillas */
    var templates = [{
        "name": "album",
        "template": "<div data-target='#album-{{id}}' data-toggle='collapse'" +
            " class='album-info collapsed'>{{name}} de {{artist}}</div><div" +
            " id='album-{{id}}' data-id='{{id}}'class='album-songs" +
            " collapse'></div"
    }, {
        "name": "song",
        "template": "<div id='song-{{id}}' data-id='{{id}}' " +
            "class='song'><span class='catalog-preview-progress'></span>" +
            "<span class='catalog-song-title'>{{name}}</span>" +
            "<span class='btn-group pull-right'>" +
            "<button data-loading-text='<i class=\"icon-loading icon-spin\">" +
            "</i>' data-playing-text='<i class=\"icon-preview-stop\"></i>' " +
            "class='btn btn-primary preview-song'><i " +
            "class='icon-preview-play'></i></button></span></div>"
    }];
    $.each(templates, function (i, template) {
        ich.addTemplate(template.name, template.template);
    });
    initCatalog();
    initOptions();
    getWindowMinSize();
    $("#player-info").collapse("show");
    getInfo();
    /* Inicializa botón para mostrar el software de terceros */
    $("#showThirdPartySoftware").click(function () {
        $(this).addClass('hide');
    });
    /* Corrige un problema de visualización al mostrar la sección software de
       terceros, refs #20 */
    $("#license-info").on("shown.bs.collapse", function () {
        $(window).resize();
    });
    /* Define mostrar nuevamente el boton "Mostrar" en la seccion software de
       terceros */
    $("#about-modal").on("hidden.bs.modal", function () {
        $(this).find(".hide").removeClass("hide");
        $("#license-info").removeClass("in");
    });
});

$(window).resize(function () {
    getWindowMinSize();
});
