/*
 * universal-html5-audio.js -- Lógica del Reproductor Universal HTML5
 *
 * @license Copyright 2013 - 2015 BreadMaker aka CTM <breadmaker@radiognu.org>
 *
 * Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
 * los términos de la Licencia Pública General GNU tal como se publica por
 * la Free Software Foundation; ya sea la versión 3 de la Licencia, o
 * (a su elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que le sea útil, pero SIN
 * NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
 * IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
 * General de GNU para más detalles.
 *
 * Debería haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa; de lo contrario escriba a la Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, EE. UU.
 */

var audio, d = new Date();
var source = "http://audio.radiognu.org/radiognu.ogg";

function formatTime() {
    time = audio.currentTime;
    if (time >= 86400) {
        var d = Math.floor(time / 86400);
        time = time - d * 86400;
        var h = Math.floor(time / 3600);
        time = time - h * 3600;
        var m = Math.floor(time / 60);
        var s = Math.floor(time % 60);
        $("#u5p-days").show().text(d + "d");
        $("#u5p-hours").show().text(h.lead0(2) + ":");
        $("#u5p-minutes").text(m.lead0(2) + ":");
        $("#u5p-seconds").text(s.lead0(2));
    } else if (time >= 3600) {
        var h = Math.floor(time / 3600);
        time = time - h * 3600;
        var m = Math.floor(time / 60);
        var s = Math.floor(time % 60);
        $("#u5p-hours").show().text(h.lead0(2) + ":");
        $("#u5p-minutes").text(m.lead0(2) + ":");
        $("#u5p-seconds").text(s.lead0(2));
    } else if (time >= 60) {
        var m = Math.floor(time / 60);
        var s = Math.floor(time % 60);
        $("#u5p-minutes").text(m.lead0(2) + ":");
        $("#u5p-seconds").text(s.lead0(2));
    } else {
        var s = Math.floor(time % 60);
        $("#u5p-days").hide();
        $("#u5p-hours").hide();
        $("#u5p-minutes").text("00:");
        $("#u5p-seconds").text(s.lead0(2));
    }
}

restartingAudioElement = false;

function addEvents(audioElement) {
    audioElement.addEventListener("abort", function () {
        consoleLog("Evento 'abort' disparado.");
    });
    audioElement.addEventListener("canplay", function () {
        consoleLog("Evento 'canplay' disparado.");
    });
    audioElement.addEventListener("canplaythrough", function () {
        consoleLog("Evento 'canplaythrough' disparado.");
    });
    audioElement.addEventListener("durationchange", function () {
        consoleLog("Evento 'durationchange' disparado.");
    });
    audioElement.addEventListener("emptied", function () {
        consoleLog("Evento 'emptied' disparado.");
    });
    audioElement.addEventListener("ended", function () {
        consoleLog("Evento 'ended' disparado.");
    });
    audioElement.addEventListener("error", function (e) {
        switch (e.target.error.code) {
        case e.target.error.MEDIA_ERR_ABORTED:
            consoleLog(
                "Evento 'error' disparado: El usuario abortó la reproducción."
            );
            break;
        case e.target.error.MEDIA_ERR_NETWORK:
            statusMsg({
                message: "Error de red",
                icon: "icon-error",
                hide: true
            });
            consoleLog("Evento 'error' disparado: Un error de red ocurrió.");
            resetPlayer();
            break;
        case e.target.error.MEDIA_ERR_DECODE:
            statusMsg({
                message: "Error al decodificar",
                icon: "icon-error",
                hide: true
            });
            consoleLog("Evento 'error' disparado: Error al decodificar.");
            break;
        case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
            consoleLog(
                "Evento 'error' disparado: El archivo no pudo ser cargado, por un error de red o por un formato no soportado."
            );
            break;
        default:
            statusMsg({
                message: "Error de reproducción",
                icon: "icon-error",
                hide: true
            });
            consoleLog("Evento 'error' disparado: Un error desconocido ocurrió.");
            break;
        }
        if (restartingAudioElement) {
            $("#u5p-status-progress").css("width", "0%").parent().removeClass(
                "active");
        }
    });
    audioElement.addEventListener("loadeddata", function () {
        consoleLog("Evento 'loadeddata' disparado.");
    });
    audioElement.addEventListener("loadedmetadata", function () {
        consoleLog("Evento 'loadedmetadata' disparado.");
    });
    audioElement.addEventListener("loadstart", function () {
        consoleLog("Evento 'loadstart' disparado.");
    });
    audioElement.addEventListener("paused", function () {
        consoleLog("Evento 'paused' disparado.");
    });
    audioElement.addEventListener("play", function () {
        consoleLog("Evento 'play' disparado.");
    });
    audioElement.addEventListener("playing", function () {
        $("#u5p-status-progress").css("width", "0%").parent().removeClass(
            "active");
        statusMsg({
            message: "Reproduciendo",
            icon: "icon-play",
            hide: true
        });
        consoleLog("Evento 'playing' disparado.");
        audio.wasPlaying = true;
    });
    audioElement.addEventListener("progress", function () {
        consoleLog("Evento 'progress' disparado.");
    });
    audioElement.addEventListener("ratechange", function () {
        consoleLog("Evento 'ratechange' disparado.");
    });
    audioElement.addEventListener("seeked", function () {
        consoleLog("Evento 'seeked' disparado.");
    });
    audioElement.addEventListener("seeking", function () {
        consoleLog("Evento 'seeking' disparado.");
    });
    audioElement.addEventListener("suspend", function () {
        consoleLog("Evento 'suspend' disparado.");
    });
    audioElement.addEventListener("stalled", function () {
        consoleLog("Evento 'stalled' disparado.");
        if (navigator.onLine && audio.wasPlaying) {
            resetPlayer();
            audio.wasPlaying = false;
            audio.play();
        }
    });
    audioElement.addEventListener("timeupdate", function () {
        consoleLog("Evento 'timeupdate' disparado.");
    });
    audioElement.addEventListener("volumechange", function () {
        consoleLog("Evento 'volumechange' disparado.");
    });
    audioElement.addEventListener("waiting", function () {
        $("#u5p-status-progress").css("width", "100%").parent().addClass(
            "active");
        statusMsg({
            message: "Cargando buffer",
            icon: "icon-loading icon-spin",
            hide: false
        });
        consoleLog("Evento 'waiting' disparado.");
    });
    restartingAudioElement = false;
}

var playerTimer;

function initPlayer(src) {
    if (document.createElement('audio').canPlayType) {
        audio = document.createElement('audio');
        if (audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"').replace(
                /no/, '')) {
            audio = $("#u5p-html5-player audio")[0];
            $("#u5p-main").addClass("visible");
            $("#u5p-minutes").text("00:");
            $("#u5p-seconds").text("00");
            $(".u5p-play").click(function () {
                if (audio.paused) {
                    if (navigator.onLine) {
                        audio.play();
                        consoleLog("Iniciando reproducción...");
                        playerTimer = setInterval(formatTime, 100);
                        $(this).addClass("icon-stop").removeClass("icon-play");
                    }
                } else {
                    resetPlayer();
                }
            });
            audio.wasPlaying = false;
            addEvents(audio);
            $("#u5p-volume-button").click(function () {
                $(this).children().toggleClass("icon-volume-up icon-volume-off");
                audio.muted = !audio.muted;
                consoleLog("audio.muted es '" + audio.muted + "'.");
            });
            $(".u5p-volume-slider").on("input", function () {
                audio.volume = $(this).val() / 100;
            });
            $("#u5p-main").on("selectstart", function (e) {
                e.preventDefault();
                return false;
            });
            $("#u5p-right-controls").hover(function () {
                $("#u5p-volume-slider").addClass("visible");
            }, function () {
                $("#u5p-volume-slider").removeClass("visible");
            });
            consoleLog("Reproductor inicializado.");
        } else {
            $("#u5p-no-js").attr("id", "u5p-cant-ogg").html($("<p/>").addClass(
                "navbar-text").text(
                "Este navegador no soporta el codec libre Ogg."));
            consoleLog("Este navegador no soporta el codec libre Ogg.");
        }
    } else {
        $("#u5p-no-js").attr("id", "u5p-cant-html5").html($("<p/>").addClass(
            "navbar-text").text(
            "Este navegador no soporta la etiqueta HTML5 audio o la tiene desactivada."
        ));
        consoleLog(
            "Este navegador no soporta la etiqueta HTML5 audio o la tiene desactivada."
        );
    }
}

function resetPlayer(src) {
    consoleLog("Reproducción detenida.");
    var audioWasPlaying = !audio.paused;
    var previousVolume = audio.volume;
    audio.pause();
    restartingAudioElement = true;
    audio.src = "";
    $("#u5p-days, #u5p-hours").empty();
    $("#u5p-minutes").text("00:");
    $("#u5p-seconds").text("00");
    clearInterval(playerTimer);
    source = src ? src : source;
    $("#u5p-html5-player").html($("<audio/>").attr({
        "src": source,
        controls: "controls",
        preload: "none",
        mozaudiochannel: "content"
    }));
    audio = $("#u5p-html5-player audio")[0];
    audio.volume = previousVolume;
    addEvents(audio);
    $("#u5p-volume-button").children().removeClass("icon-volume-off").addClass(
        "icon-volume-up");
    $(".u5p-play.icon-stop").removeClass("icon-stop").addClass("icon-play");
    if (audioWasPlaying && src != undefined)
        audio.play();
    consoleLog("Reproductor reiniciado.");
}

Number.prototype.lead0 = function (n) {
    var nz = "" + this;
    while (nz.length < n) {
        nz = "0" + nz;
    }
    return nz;
};

function showNow() {
    d = new Date();
    return d.getDate().lead0(2) + "/" + (d.getMonth() + 1).lead0(2) + "/" + d.getFullYear() +
        " " + d.getHours().lead0(2) + ":" + d.getMinutes().lead0(2) + ":" + d.getSeconds()
        .lead0(2) + "." + d.getMilliseconds().lead0(3);
}

function consoleLog(msg) {
    if (window.hasOwnProperty("depurarU5A") && window.depurarU5A)
        console.debug(showNow() + " : " + msg);
}

$(document).ready(function () {
    initPlayer(source);
    console.info(
        'Ejecute el comando "var depurarU5A = true;" para mostrar los mensajes de depuración'
    );
});
